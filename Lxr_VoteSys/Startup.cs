using Lxr_VoteSys.Models.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace qimo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {    //注入数据库上下文
            services.AddDbContext<Lxr_companyContext>();
            services.AddControllers();

            //添加cors
            services.AddCors(options =>
            {
                //添加core 策略
                options.AddPolicy("Policy1", //策略名
                    builder =>
                    {
                        builder.WithOrigins("*") //表示可以被所有地址跨域访问 允许跨域访问的地址，不能以正反斜杠结尾。
                            .SetIsOriginAllowedToAllowWildcardSubdomains()//设置策略里的域名允许通配符匹配，但是不包括空。
                                                                          //例：http://localhost:3001 不会被通过
                                                                          // http://xxx.localhost:3001 可以通过
                            .AllowAnyHeader()//配置请求头
                            .AllowAnyMethod();//配置允许任何 HTTP 方法访问
                    });
            });
            //添加swagger
            services.AddSwaggerGen(options =>
            {
                options.CustomSchemaIds(x => x.FullName);
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
                // 获取xml文件名
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                // 获取xml文件路径
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                // 添加控制器层注释，true表示显示控制器注释
                options.IncludeXmlComments(xmlPath, true);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            //使用cors，注意中间件的位置位于UseRouting与UseAuthorization之间
            app.UseCors("Policy1");//此处如果填写了app.UserCors("Policy1")，则控制器中默认使用该策略，并且不需要在控制器上添加[EnableCors("Policy1")]
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "coreMVC3.1");
                //c.RoutePrefix = string.Empty;
                c.RoutePrefix = "API";     //如果是为空 访问路径就为 根域名/index.html,注意localhost:端口号/swagger是访问不到的
                                           //路径配置，设置为空，表示直接在根域名（localhost:8001）访问该文件
                                           // c.RoutePrefix = "swagger"; // 如果你想换一个路径，直接写名字即可，比如直接写c.RoutePrefix = "swagger"; 则访问路径为 根域名/swagger/index.html
            });
        }
    }
}
