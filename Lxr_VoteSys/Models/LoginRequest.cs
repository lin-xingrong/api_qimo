﻿namespace Lxr_VoteSys.Models
{
    public class LoginRequest
    {
        public string Username { get; internal set; }
        public string Userpassword { get; internal set; }
    }
}