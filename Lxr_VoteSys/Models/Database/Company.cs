﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lxr_VoteSys.Models.Database
{
    public partial class Company
    {
        public int ComId { get; set; }
        public string Companyname { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
        public string Honortime { get; set; }
        public string Honor { get; set; }
        public string Honorimg { get; set; }

        
    }
}
