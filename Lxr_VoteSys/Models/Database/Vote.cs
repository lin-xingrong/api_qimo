﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lxr_VoteSys.Models.Database
{
    public partial class Vote
    {
        public int? VoteId { get; set; }
        public string Logo { get; set; }
        public string Companyname { get; set; }
        public string Number { get; set; }
    }
}
