﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Lxr_VoteSys.Models.Database
{
    public partial class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Userpassword { get; set; }
    }
}
