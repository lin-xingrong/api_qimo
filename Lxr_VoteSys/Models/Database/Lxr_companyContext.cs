﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Lxr_VoteSys.Models.Database
{
    public partial class Lxr_companyContext : DbContext
    {
        public Lxr_companyContext()
        {
        }

        public Lxr_companyContext(DbContextOptions<Lxr_companyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=Lxr_company;User ID=sa;Password=lll33322");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_PRC_CI_AS");

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("company");

                entity.Property(e => e.ComId).HasColumnName("comID");

                entity.Property(e => e.Companyname).HasColumnName("companyname");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Honor).HasColumnName("honor");

                entity.Property(e => e.Honorimg).HasColumnName("honorimg");

                entity.Property(e => e.Honortime)
                    .HasColumnType("datetime")
                    .HasColumnName("honortime");

                entity.Property(e => e.Logo).HasColumnName("LOGO");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("user");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username");

                entity.Property(e => e.Userpassword)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("userpassword")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Vote>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vote");

                entity.Property(e => e.Companyname).HasColumnName("companyname");

                entity.Property(e => e.Logo).HasColumnName("LOGO");

                entity.Property(e => e.Number).HasColumnName("number");

                entity.Property(e => e.VoteId).HasColumnName("voteID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
