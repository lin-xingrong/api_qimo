﻿namespace Lxr_VoteSys.Models
{
    public class AddUserRequest
    {
        public string Username { get; internal set; }
        public object Userpassword { get; internal set; }
        public object CirPassword { get; internal set; }
    }
}