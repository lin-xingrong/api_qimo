﻿namespace Lxr_VoteSys.Models
{
    public class AddcompanyRequest
    {
      
        public string Companyname { get; set; }
        public string ComId { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
        public string Honortime { get; set; }
        public string Honor { get; set; }
        public string Honorimg { get; set; }
    }
}