﻿using Lxr_VoteSys.Models;
using Lxr_VoteSys.Models.Database;
using Lxr_VoteSys.Service;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Lxr_VoteSys.Controllers
{
    /// <summary>
    /// 是
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        //注入jwt服务
        private readonly IJWTService _jwtService;
        private readonly Lxr_companyContext _db;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jwtService"></param>
        public LoginController(IJWTService jwtService, Lxr_companyContext db)
        {
            _jwtService = jwtService;
            _db = db;
        }

        /*/// <summary>
        /// 创建token  
        /// 授权 找保安拿钥匙
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string CreateToken(string name)
        {
            //调用jwt服务里的创建token方法
            return _jwtService.CreateToken(name, 0);
        }

        /// <summary>
        /// 认证 门锁对钥匙进行认证
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string UserInfo()
        {
            //门锁 告诉我 我是谁 
            //获取用户信息 登录用户的信息 认证信息
            return Response.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;
        }*/


        //注册 添加一个新用户 
        /// <summary>
        /// 注册接口
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string AddUser(AddUserRequest request)
        {
            //注册逻辑

            //检查用户名是否存在
            //查询有没有这个用户名
            if (_db.Users.Any(x => x.Username == request.Username))
            {
                return "用户名已存在";
            }
            if (request.Userpassword != request.CirPassword)
                return "两次密码不一致";

            //添加一个新的用户
            //定义一个用户对象
            var user = new User()
            {
               
                Username = request.Username,
                Userpassword = (string)request.Userpassword,
                
            };
            _db.Users.Add(user);
            var row = _db.SaveChanges();
            if (row > 0)
                return "注册成功";
            return "注册失败";
        }
        //用户登录
        [HttpPost]
        public string Login(LoginRequest request)
        {
            //怎么用参数

            //登录 - 校验账号密码是否正确  
            //查询这个用户名 下的用户
            var user = _db.Users.FirstOrDefault(x => x.Username == request.Username);
            if (user == null)
                return "账号不存在";
            //校验密码
            if (user.Userpassword == request.Userpassword)
            {
                //成功 
                //创建钥匙 给用户
                return _jwtService.CreateToken(user.Username, user.Id);
            }
            return "密码不正确";
            var token = _jwtService.CreateToken(user.Id, (string)user.Userpassword);

            //返回token
            return token;
        }
    }
}
