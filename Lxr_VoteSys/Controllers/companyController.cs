﻿using Lxr_VoteSys.Models.Database;
using Microsoft.AspNetCore.Mvc;

namespace Lxr_VoteSys.Controllers
{
    //api/Shop/方法名字
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class companyController : Controller
    {
        //配置访问路由
        [Route("api/[controller]/[action]")]
        [ApiController]
        public class GoodController : ControllerBase
        {
            //定义数据库上下文
            private readonly Lxr_companyContext _db;

            /// <summary>
            /// 构造方法 构造注入
            /// </summary>
            public GoodController(Lxr_companyContext db)
            {
                _db = db;
            }
            /// <summary>
            /// 新增企业信息
            /// </summary>
            [HttpGet]
            public string AddShop(Models.AddcompanyRequest request)
            {
                //后端业务逻辑  

                //把单个的请求参数  转换成 数据的类
                var company = new Company
                {
                    Companyname = request.Companyname,
                    Logo = request.Logo,
                    Description = request.Description,
                    Honortime = request.Honortime,
                    Honor = request.Honor,
                    Honorimg = request.Honorimg
                };
                _db.Companies.Add(company);
                var row = _db.SaveChanges();
                if (row > 0)
                    return "新增成功";
                return "新增失败";
            }
        }
    }
}
