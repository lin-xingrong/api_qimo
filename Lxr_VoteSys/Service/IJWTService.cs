﻿namespace Lxr_VoteSys.Service
{
    public interface IJWTService
    {
        string CreateToken(int Id, string Username);
        string CreateToken(string Username, int Id);
        string CreateToken(int id, object nickName);
    }
}